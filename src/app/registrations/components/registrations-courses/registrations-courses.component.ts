import {Component, Injectable, OnInit, OnDestroy, ViewChild, TemplateRef} from '@angular/core';
import { CurrentRegistrationService } from '../../services/currentRegistrationService.service';
import { ProfileService } from '../../../profile/services/profile.service';
import {TranslateService} from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { Router } from '@angular/router';
import { LoadingService } from '@universis/common';
import {RegistrationSpecialtyComponent} from '../registration-specialty/registration-specialty.component';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {RegistrationsCourseClassComponent} from '../registrations-course-class/registrations-course-class.component';
import { ConfigurationService } from '@universis/common';
// tslint:disable-next-line:max-line-length
import {RegistrationsStudyProgramOptionsComponent} from '../registrations-study-program-options/registrations-study-program-options.component';
import { LocalizedDatePipe } from '@universis/common';

@Component({
    selector: 'app-registrations-courses',
    templateUrl: 'registrations-courses.component.html',
    styleUrls: ['registrations-courses.component.scss']

})

export class RegistrationCoursesComponent implements OnInit {
    // student's department
    private department: any;

    // current registration. Contains courses already registered by the student
    public currentRegistration: any;

    // courses available for the student to register
    public availableCourses: any;

    // courses available for the student to register
    public allAvailableCourses: any;

    // should allow user to hide all non-validated classes
    public canShowValidatedCourses: boolean;
    public shouldOnlyShowValidatedCourses: boolean;

    // code for the shared messagebox
    public code: string;

    // Registration Effective Status
    public effectiveStatus: any;

    public totalUnits = 0;
    public totalEcts = 0;

    // Groups of semesters
    public semesters: any;
    // Groups of course types
    public courseTypeGroups: any;

    // this attribute is equal to either semesters or courseTypeGroups
    // depending on the grouping choice
    public groups: any;
    // boolean: true if group by semester, false if group by type
    public semesterGroup = true;

    public canRegister = false;

    private registrationPeriodStart: Date;
    private registrationPeriodEnd: Date;
    public start: string;
    public end: string;

    public openRegistrationPeriod = false;
    private periodNotOpened: boolean;
    private periodPassed: boolean;
    public programGroups: any;


    // param to pass to the translation of the period
    public currentPeriod;
    public selectedElement: any;
    public footerIsOpen = false ;
    public loading = true;

    public selectSpecialtyModalRef: BsModalRef;
    public classSectionModalRef: BsModalRef;

    public currentLanguage;
    public defaultLanguage = null;
  // boolean to check if student has edited his registration
  public registrationEdited = false;
  @ViewChild('modalTemplate') modalTemplate: TemplateRef<any>;
  public modalRef: any;
  public filterGroup = true;

    constructor(private profileService: ProfileService,
                private currRegService: CurrentRegistrationService,
                private translate: TranslateService,
                private _context: AngularDataContext,
                private router: Router,
                private loadingService: LoadingService,
                private modalService: BsModalService,
                private _configurationService: ConfigurationService) {

        this.currentLanguage = this._configurationService.currentLocale;
        this.defaultLanguage = this._configurationService.settings.i18n.defaultLocale;
    }

    // This function will initialize department, availableClasses and currentRegistration
    async ngOnInit() {
      this.loading = true;
      this.loadingService.showLoading();  // show loading
      try {
        this.effectiveStatus = await this.currRegService.getCurrentRegistrationEffectiveStatus();
        // get available program groups
        try {
          this.programGroups = await this.currRegService.getAvailableProgramGroups();
        } catch (err) {
          console.error(err);
        }
        if (this.effectiveStatus.code === 'SELECT_SPECIALTY') {
          // set component code to effective status code
          this.code = this.effectiveStatus.code;
          // set can register flag to false
          this.canRegister = false;
        } else if (this.effectiveStatus.status === 'open' && sessionStorage['edit'] !== 'TRUE'
                    && this.programGroups && this.programGroups.length === 0) {
          this.router.navigate(['/registrations/courses/checkout']);
          return;
        }

        // get student details
        const student = await this.profileService.getStudent();
        // get student department
        this.department = student.department;

        this.registrationPeriodStart = this.department.registrationPeriodStart;
        this.registrationPeriodEnd = this.department.registrationPeriodEnd;

        if (this.registrationPeriodStart && this.registrationPeriodEnd) {
          this.start = (this.registrationPeriodStart.getDate().toString() + '/' +
            + (this.registrationPeriodStart.getMonth() + 1).toString() + '/' +
            + this.registrationPeriodStart.getFullYear());

          this.end = (this.registrationPeriodEnd.getDate().toString() + '/' +
            + (this.registrationPeriodEnd.getMonth() + 1).toString() + '/' +
            + this.registrationPeriodEnd.getFullYear());
        } else {
          this.start = this.end = null;
        }

        this.currentPeriod = {
          period: this.translate.instant(this.department.currentPeriod.alternateName),
          year: this.department.currentYear.alternateName
        };

        // Check whether we are between registration period deadlines
        this.checkRegistrationPeriodDeadlines();
        // Check the registration period effective status
        this.checkRegistrationStatus(this.effectiveStatus);
        // Check if we can filter by validated courses
        this.checkCanShowValidatedCourses();

        this.currentRegistration = await this.currRegService.getCurrentRegistration();
        this.registrationEditedStatus(this.currentRegistration);
        if (this.effectiveStatus.status === 'open') {
          // get the available classes for this registration session
          this.availableCourses = await this.currRegService.getAvailableClasses();
          this.allAvailableCourses = this.availableCourses ;

          // If there are courses in the registration
          if (this.currentRegistration.classes) {
            // add an extra attribute: isRegisteredCourse to all the available
            // classes in order to distinguish between the already registered
            // ones, ie the ones contained in the currentRegistration
            this.addRegistrationAttribute();

            // initialize the ects displayed at bottom window
            this.initializeEcts();
          }
          // initialize the grouping by semester
          this.groupBySemester();
        }
        // hide loading
        this.loadingService.hideLoading();
        this.loading = false;
      } catch (error) {
        this.loadingService.hideLoading();
        this.loading = false;
        console.log(error.message);
      }
    }

    // Function to check the Registration status and show appropriate messages at the screen
    checkRegistrationStatus(effectiveStatus) {
      // Set the code property of this component equal to the
      // code property of the returned status.
      // This allows us to create a messagebox
      // at the HTML file(registrations-courses.component.html)
      // with the parameters of the messagebox(icon, message, etc...)
      // taken from the 'registrations.el.json' file using
      // this code as a key
      this.code = this.effectiveStatus.code;
      if (this.effectiveStatus.status === 'open') {
        this.openRegistrationPeriod = true;
        if (this.effectiveStatus.code === 'OPEN_NO_TRANSACTION' && sessionStorage['edit'] === 'TRUE') {
          sessionStorage['edit'] = 'FALSE';
          this.canRegister = true;
        }
        if (this.effectiveStatus.code !== 'OPEN_NO_TRANSACTION'
          && this.effectiveStatus.code !== 'OPEN_NO_REGISTRATION'
          && this.effectiveStatus.code !== 'SELECT_SPECIALTY') {
          // The first time this component is loaded it will redirect to checkout
          // From checkout if we hit 'edit' we must come back here, so we
          // add a variable stay the first time we come and read it the next times
          // so we now we must stay in this page.
          // NOTE: A better routing design is needed here!
          if (sessionStorage['edit'] === 'TRUE') {
            sessionStorage['edit'] = 'FALSE';
            this.canRegister = true;
          } else {
            if (this.programGroups && this.programGroups.length === 0) {
              this.loading = false; // Data is loaded
              this.loadingService.hideLoading(); // hide loading
              this.router.navigate(['/registrations/courses/checkout']);
            }
          }
        }
      } else if (this.effectiveStatus.status === 'closed') {
        if (this.effectiveStatus.code === 'CLOSED_NO_TRANSACTION' ||
          this.effectiveStatus.code === 'CLOSED_NO_REGISTRATION' ||
          this.effectiveStatus.code === 'CLOSED_PERIOD_NO_TRANSACTION') {

          // check department flag
          if (this.department.isRegistrationPeriod) {
            // Then check component flag to see if current date
            // is between registration period deadlines and show the
            // appropriate message('Not opened yet' vs 'Deadline passed')
            if (this.periodNotOpened) {
              this.code = 'NOT_OPENED_YET';
            } else if (this.periodPassed) {
              this.code = 'PASSED';
            } else {
              this.code = 'CLOSED_GENERAL';
            }
          } else {
            this.code = 'CLOSED_GENERAL';
          }
        }
      }
    }

    // Function triggered when user clicks on one of the available courses
    async registerForCourse(course) {

   //   this.registrationEdited = true;
      // set state of registered 1 because registered from student
      course.autoRegistered = 1;
      if (course.mustRegisterSection === 1) {
        const initialState = {
          course: course
        };
        this.modalService.show(RegistrationsCourseClassComponent, {initialState});
        this.modalService.onHide.subscribe(() => {
          if (course.isRegisteredCourse) {
            this.currentRegistration = JSON.parse(sessionStorage['RegistrationLatest']);
            this.totalUnits += course.units;
            this.totalEcts += course.ects;
          }
        });
      } else {
        // call the service method to register for course
        this.currRegService.registerForCourse(course).then(res => {
          // change the attribute of the course to "true"
          // Note: In JS arguements are passed by reference so
          // a change here will change the original course too
          course.isRegisteredCourse = true;

          // update the added courses array to show the new added course
          // at the bottom window. The service function called above will
          // save the new course to session storage so we can take it from there
          this.currentRegistration = JSON.parse(sessionStorage['RegistrationLatest']);
          this.registrationEditedStatus(this.currentRegistration);
          // Add the course units and ects to the total sum
          this.totalUnits += course.units;
          this.totalEcts += course.ects;
        });
      }
    }

    removeCourse(course) {
        this.currRegService.removeCourse(course).then(() => {
            // change the attribute of the course to "false"
            // Note: In JS arguements are passed by reference so
            // a change here will change the original course too
            course.isRegisteredCourse = false;
            course.section = 0;
            course.sectionName = null;

            // update the added courses array to show the new added course
            // at the bottom window. The service function called above will
            // save the new course to session storage so we can take it from there
            this.currentRegistration = JSON.parse(sessionStorage['RegistrationLatest']);
          this.registrationEditedStatus(this.currentRegistration);
            // Subtract the course units and ects from the total sum
            this.totalUnits -= course.units;
            this.totalEcts -= course.ects;
        });
    }

    // check all the available courses to see which of these are already
    // registered and set a new attribute "isRegisteredCourse" accordingly
    addRegistrationAttribute() {
        // for every available course
        for (let i = 0; i < this.availableCourses.length; ++i) {
          this.allAvailableCourses[i]['isRegisteredCourse'] = false;
          this.allAvailableCourses[i]['isLocked'] = false;

            // check at current registration(check by course class)
            for (let j = 0; j < this.currentRegistration.classes.length; ++j) {
              if (this.allAvailableCourses[i].courseClass.id === this.currentRegistration.classes[j].courseClass.id) {
                this.allAvailableCourses[i]['isRegisteredCourse'] = true;
                this.allAvailableCourses[i].section = this.currentRegistration.classes[j].section;
                this.allAvailableCourses[i].sectionName = this.currentRegistration.classes[j].sectionName;

                // Check whether or not the course can be removed
                // If allowRemoveAutoRegisteredCourseClass is false or
                // selected class is autoRegistered status -1
                // Then the course cannot be removed from the current registration
                 if (!this.department.organization.instituteConfiguration.allowRemoveAutoRegisteredCourseClass
                   && this.currentRegistration.classes[j].autoRegistered !== 1) {
                   this.allAvailableCourses[i]['isLocked'] = true;
                 }
                 break;
              }
            }
        }
    }

    checkRegistrationPeriodDeadlines(): void {
        const today = new Date();
        if (today < this.registrationPeriodStart) {
            this.periodNotOpened = true;
        } else if (today > this.registrationPeriodEnd) {
            this.periodPassed = true;
        } else {
            this.periodNotOpened = this.periodPassed = false;
        }
    }

    performDefaultAction(action= '') {
      if (this.effectiveStatus.status === 'open') {
        let config = {
          animated: true,
          class: 'modal-dialog-centered modal-xl border-0',
          ignoreBackdropClick: true
        };
        if (this.effectiveStatus.code === 'SELECT_SPECIALTY' && action === '') {
          this.selectSpecialtyModalRef = this.modalService.show(RegistrationSpecialtyComponent, config);
        } else {
          if (action === 'selectGroups') {
            config = Object.assign(config, {'initialState': {'programGroups' : this.programGroups}});
            this.modalRef = this.modalService.show(RegistrationsStudyProgramOptionsComponent,
              config);
          } else {
            this.skipToCourseRegistration();
          }
        }
      }
    }

    skipToCourseRegistration() {
        this.canRegister = true;
    }

    filterBySemester(semester) {
      this.selectedElement = semester;

      this.availableCourses = this.allAvailableCourses.filter(x => {
        return x.semester.id === this.selectedElement
        && ((this.shouldOnlyShowValidatedCourses && x.validationResult.every(y => y.success === true))
        || !this.shouldOnlyShowValidatedCourses);
      });
      // notation used by other groups
      this.groups = [{courses: this.availableCourses}];
    }
    filterByCourseType(course) {
      this.selectedElement = course;

      this.availableCourses = this.allAvailableCourses.filter(x => {
        return x.courseType.name === this.selectedElement
        && ((this.shouldOnlyShowValidatedCourses && x.validationResult.every(y => y.success === true))
        || !this.shouldOnlyShowValidatedCourses);
        });

      // notation used by other groups
      this.groups = [{courses: this.availableCourses}];
  }



  changefooterIsOpen(parametr) {
        this.footerIsOpen = parametr;
    }

    /**
     *
     * @param {string} groupByAttribute
     */
    getGroupData(groupByAttribute) {
        if ( sessionStorage[`${groupByAttribute}GroupData`]) {
            return new Promise( (resolve, reject) => {
                resolve(JSON.parse(sessionStorage[`${groupByAttribute}GroupData`]));
            });
        } else {
        return this._context.model('students/me/availableClasses')
            .select(groupByAttribute, `count(${groupByAttribute}) as count`)
            .groupBy(groupByAttribute)
            .orderBy(groupByAttribute)
            .getItems().then( groups => {
                sessionStorage[`${groupByAttribute}GroupData`] = JSON.stringify(groups);
                return groups;
            });
        }
    }

    groupByCourseType() {
        this.getGroupData('courseType').then(groups => {
            this.courseTypeGroups = groups;
            this.courseTypeGroups.forEach(x => {
                x['courses'] = this.allAvailableCourses.filter(course => {
                    return x.courseType.id === course.courseType.id
                    && ((this.shouldOnlyShowValidatedCourses && course.validationResult.every(y => y.success === true))
                    || !this.shouldOnlyShowValidatedCourses);
                });
            });
            this.groups = this.courseTypeGroups;
            this.semesterGroup = false;
        });
    }

    groupBySemester() {
        this.getGroupData('semester').then(groups => {
            this.semesters = groups;
            this.semesters.forEach(x => {
                x['courses'] = this.allAvailableCourses.filter(course => {
                    return x.semester.id === course.semester.id
                    && ((this.shouldOnlyShowValidatedCourses && course.validationResult.every(y => y.success === true))
                    || !this.shouldOnlyShowValidatedCourses);
                });
            });
            this.groups = this.semesters;
            this.semesterGroup = true;
      });
    }

    onChangeGroup(selected: any) {
        if (selected === 'semester') {
          this.removeCourseTypeFiltering();
          this.groupBySemester();

        } else {
          this.removeSemesterFiltering();
          this.groupByCourseType();

        }
    }

    removeSemesterFiltering() {
        this.groups = this.semesters;
        this.selectedElement = null;
    }
    removeCourseTypeFiltering() {
      this.groups = this.courseTypeGroups;
      this.selectedElement = null;
  }


  toggleBlock(item: any) {
        item.isHidden = !item.isHidden;
    }

    // initialize total units and ects from added courses
    initializeEcts() {
        for (let i = 0; i < this.currentRegistration.classes.length; ++i) {
            this.totalUnits += this.currentRegistration.classes[i].units;
            this.totalEcts += this.currentRegistration.classes[i].ects;
        }
    }

    checkCanShowValidatedCourses() {
      // ping server for services
      if (this.canShowValidatedCourses == null) {
        this._context.getService().execute({
            method: 'GET',
            url: 'diagnostics/services',
            headers: { },
            data: null
        }).then(services => {
          // if return value is an array of services
          if (Array.isArray(services)) {
            // search for class validator
            this.canShowValidatedCourses = services.find( service => {
                return service.serviceType === 'ValidateAvailableClassesAction';
            }) != null;
            return this.canShowValidatedCourses;
          }
          console.error('Could not retrieve user services');
        });

      if (sessionStorage['showOnlyValidatedCoursesPreference'] !== undefined) {
        this.shouldOnlyShowValidatedCourses = JSON.parse(sessionStorage['showOnlyValidatedCoursesPreference']);
      } else {
        this.shouldOnlyShowValidatedCourses = true;
      }
    }
  }

  filterOnlyValidatedCourses() {
    // Save preference to sessionStorage
    sessionStorage['showOnlyValidatedCoursesPreference'] = JSON.stringify(this.shouldOnlyShowValidatedCourses);
    if (this.selectedElement) {
      if (this.semesterGroup) {
        this.filterBySemester(this.selectedElement);
      } else {
        this.filterByCourseType(this.selectedElement);
      }
    } else {
      if (this.semesterGroup) {
        this.groupBySemester();
      } else {
        this.groupByCourseType();
      }
    }
  }

  registrationEditedStatus(currentRegistration: any ) {
    if (JSON.stringify(currentRegistration.classes) === sessionStorage['InitialRegistration']) {
      this.registrationEdited = false;
    } else {
      this.registrationEdited = true;
    }
  }

  openFilterModal() {
      this.modalRef = this.modalService.show(this.modalTemplate);
  }

  closeModal(group: boolean) {
    this.modalRef.hide();
    if (group) {
      this.onChangeGroup('semester');
    } else {
      this.onChangeGroup('courseType');
    }
  }

}
