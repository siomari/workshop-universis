import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { ConfigurationService } from '@universis/common';
import { BsModalRef } from 'ngx-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CurrentRegistrationService } from '../../services/currentRegistrationService.service';
import { MostModule} from '@themost/angular';
import { ProfileService } from '../../../profile/services/profile.service';
import { RouterTestingModule } from '@angular/router/testing';

import { RegistrationsCourseClassComponent} from './registrations-course-class.component';
import {TestingConfigurationService} from '../../../test';

describe('RegistrationSpecialtyComponent', () => {
  // tslint:disable-next-line:prefer-const
  let component: RegistrationsCourseClassComponent;
  // tslint:disable-next-line:prefer-const
  let fixture: ComponentFixture<RegistrationsCourseClassComponent>;

  beforeEach(async(() => {
    return TestBed.configureTestingModule({
      declarations: [ RegistrationsCourseClassComponent ],
      imports: [ HttpClientTestingModule,
        RouterTestingModule,
        FormsModule,
        TranslateModule.forRoot(),
        MostModule.forRoot({
          base: '/',
          options: {
            useMediaTypeExtensions: false
          }
        })],
      providers: [
        BsModalRef,
        {
          provide: ConfigurationService,
          useClass: TestingConfigurationService
        },
        CurrentRegistrationService,
        ProfileService
      ]
    })
      .compileComponents();
  }));

});
