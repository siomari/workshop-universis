import {Component, OnInit} from '@angular/core';
import { UserService } from '@universis/common';
import { ProfileService } from '../../services/profile.service';
import { LoadingService } from '@universis/common';
import {AngularDataContext} from '@themost/angular';


@Component({
    selector: 'app-profile-preview',
    templateUrl: 'profile-preview.component.html',
    styleUrls: ['profile-preview.component.scss']
})

export class ProfilePreviewComponent implements OnInit {

    // student object coming from the API
    public student: any;
    public loading = true;   // Only if data is loaded
    public programGroups;

    constructor(private _userService: UserService,
        private _profileService: ProfileService,
        private _context: AngularDataContext,
        private loadingService: LoadingService) {
    }

   async ngOnInit() {
     this.loadingService.showLoading();  // show loading
     const res = await this._profileService.getStudent();
     // get program groups
     this.programGroups = await this.getProgramGroups();
     this.student = res; // Load data
     this.loading = false; // Data is loaded
     this.loadingService.hideLoading(); // hide loading
   }

  async getProgramGroups() {
    return this._context.model('students/me/programGroups').asQueryable().expand('programGroup').getItems();
  }
}
